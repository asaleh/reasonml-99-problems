let testPrint = (result) => if (result) {
  Js.log({j| Ok |j});
  true;
} else {
  Js.log({j| Fail |j});
  false;
}
